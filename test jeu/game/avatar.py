from condition import *

class Avatar(object):

    def __init__(self,
        POS_X, POS_Y, MOVE_X, MOVE_Y, x_movement, y_movement, FRAME_COUNT,
        frame, _direction, _attack, _attack_kunai, _slide, _kunai, DEGREES
    ):
        self.POS_X = POS_X
        self.POS_Y = POS_Y
        self.MOVE_X = MOVE_X
        self.MOVE_Y = MOVE_Y
        self.x_movement = x_movement
        self.y_movement = y_movement
        self.FRAME_COUNT = FRAME_COUNT
        self.frame = frame
        self._direction = _direction
        self._attack = _attack
        self._attack_kunai = _attack_kunai
        self._slide = _slide
        self._kunai = _kunai
        self.DEGREES = DEGREES

        self.p_x = self.POS_X
        self.p_y = self.POS_Y
        self.degrees = self.DEGREES

    def display(self, DISPLAY, DISPLAY_H):
        self.condition = Condition(
            self.frame, self.x_movement, self._direction, self._attack,
            self._attack_kunai, self._slide, self.p_y, self.p_x, self.POS_Y, self.POS_X
        )
        self.condition.direction(self.FRAME_COUNT, DISPLAY)
        DISPLAY.blit(self.condition.avatar,(self.condition.p_x,self.condition.p_y))
        if self.frame == self.FRAME_COUNT - 1:
            self._attack = False
            self._attack_kunai = False

        self.p_x += self.x_movement
        self.p_y += self.y_movement

        self.frame += 1
        if self.frame > self.FRAME_COUNT - 1:
            self.frame = 0

        if self.p_y >= DISPLAY_H - 140:
            self.p_y = self.POS_Y
            self.y_movement = 0
        if self.p_y < DISPLAY_H - 300:
            self.y_movement -= self.MOVE_Y

    def thrown_weapon(self, DISPLAY, DISPLAY_W):
        if self._kunai:
            rotate_kunai(DISPLAY, self.kunai_x, self.kunai_y, KUNAI, self.degrees)
            if self._direction:
                self.kunai_x += 200
                self.degrees += 75
                if self.degrees >= 270:
                    self.degrees = 270
                if self.kunai_x >= DISPLAY_W:
                    self.kunai_x = DISPLAY_W
                    self._kunai = False
                    self.kunai_x = self.KUNAI_X
                    self.degrees = self.DEGREES
            else:
                self.kunai_x -= 200
                self.degrees -= 75
                if self.degrees <= 90:
                    self.degrees = 90
                if self.kunai_x <= 0:
                    self.kunai_x = 0
                    self._kunai = False
                    self.kunai_x = self.KUNAI_X
                    self.degrees = self.DEGREES
