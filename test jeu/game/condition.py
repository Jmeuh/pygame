from sprite import *
from weapons import *

class Condition(object):

    def __init__(
        self, frame, x_movement, _direction, _attack, _attack_kunai,
        _slide, p_y, p_x, POS_Y, POS_X
    ):
        self.frame = frame
        self.x_movement = x_movement
        self._direction = _direction
        self._attack = _attack
        self._attack_kunai = _attack_kunai
        self._slide = _slide
        self.p_y = p_y
        self.p_x = p_x
        self.POS_Y = POS_Y
        self.POS_X = POS_X
        self.avatar = player_idle.imgs_reverse(self.frame, 50, 90)


    def direction(self,
        FRAME_COUNT, DISPLAY
    ):
        if self._direction:
            self.avatar = player_idle.imgs(self.frame, 50, 90)
            if self.x_movement > 0 and self.p_y == self.POS_Y:
                self.avatar = player_run.imgs(self.frame, 75, 90)
                if self._slide:
                    self.avatar = player_slide.imgs(self.frame, 80, 75)
                    self.p_y += 24
                    if self._attack or self._attack_kunai:
                        self.p_y = self.POS_Y
                        self.x_movement = 0
            if self.p_y < 575:
                self.avatar = player_jump.imgs(self.frame, 75, 107)
            if self._attack:
                if self.p_y < self.POS_Y:
                    self.avatar = player_jump_attack.imgs(self.frame, 105, 100)
                else:
                    self.avatar = player_attack.imgs(self.frame, 105, 100)
                if self.frame == FRAME_COUNT - 1:
                    self._attack = False
            if self._attack_kunai:
                self.avatar = player_throw.imgs(self.frame, 75, 90)
                if self.frame == FRAME_COUNT - 1:
                    self._attack_kunai = False
                if self.p_y < self.POS_Y:
                    self.avatar = player_jump_throw.imgs(self.frame, 75, 90)

        else:
            self.avatar = player_idle.imgs_reverse(self.frame, 50, 90)
            if self.x_movement < 0 and self.p_y == self.POS_Y:
                self.avatar = player_run.imgs_reverse(self.frame, 75, 90)
                if self._slide:
                    self.avatar = player_slide.imgs_reverse(self.frame, 80, 75)
                    self.p_y += 24
                    if self._attack or self._attack_kunai:
                        self.p_y = self.POS_Y
                        self.x_movement = 0
            if self.p_y < 575:
                self.avatar = player_jump.imgs_reverse(self.frame, 75, 107)
            if self._attack:
                if self.p_y < self.POS_Y:
                    self.avatar = player_jump_attack.imgs_reverse(self.frame, 105, 100)
                else:
                    self.avatar = player_attack.imgs_reverse(self.frame, 105, 100)
                if self.frame == FRAME_COUNT - 1:
                    self._attack = False
            if self._attack_kunai:
                self.avatar = player_throw.imgs_reverse(self.frame, 75, 90)
                if self.frame == FRAME_COUNT - 1:
                    self._attack_kunai = False
                if self.p_y < self.POS_Y:
                    self.avatar = player_jump_throw.imgs_reverse                 (self.frame, 75, 90)
            # return player
