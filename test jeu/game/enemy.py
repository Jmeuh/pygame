from avatar import *

class Enemy(Avatar):

    def __init__(self,
        POS_X, POS_Y, MOVE_X, MOVE_Y, x_movement, y_movement, FRAME_COUNT,
        frame, _direction, _attack, _attack_kunai, _slide, _kunai, DEGREES
    ):
        Avatar.__init__(self,
            POS_X, POS_Y, MOVE_X, MOVE_Y, x_movement, y_movement, FRAME_COUNT,
            frame, _direction, _attack, _attack_kunai, _slide, _kunai, DEGREES
        )

    def event(self, e):
        self.x_movement = -self.MOVE_X
        self._direction = False
        
