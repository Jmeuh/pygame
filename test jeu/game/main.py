import pygame as pg
import time
import sys
from condition import *
from player import *
from enemy import *


GREEN = (39, 155, 42)
BLUE = (113, 177, 227)

pg.init()

DISPLAY_W = 1280
DISPLAY_H = 720

DISPLAY = pg.display.set_mode((DISPLAY_W, DISPLAY_H))
CLOCK = pg.time.Clock()
pg.display.set_caption("Test Jeu")

def main():

    player = Player(
        610, 575, 13, -13, 0, 0, 20, 0,
        True, False, False, False, False, 180
    )

    enemy = Enemy(
        DISPLAY_W, 575, 13, -13, 0, 0, 20, 0,
        True, False, False, False, False, 180
    )

    while True:
        for e in pg.event.get():
            if e.type == pg.QUIT or (e.type == pg.KEYDOWN and e.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()
            player.event(e)
            enemy.event(e)
            # if enemy.p_x == player.p_x:
            #     enemy._attack = True

        DISPLAY.fill(BLUE)
        pg.draw.rect(DISPLAY, GREEN, (0, 660, 1280, 60))
        player.display(DISPLAY, DISPLAY_H)
        enemy.display(DISPLAY, DISPLAY_H)

        player.thrown_weapon(DISPLAY, DISPLAY_W)
        enemy.thrown_weapon(DISPLAY, DISPLAY_W)

        pg.display.update()
        CLOCK.tick(30)

main()
