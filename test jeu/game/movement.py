import pygame as pg

class Sprite(object):

    def __init__(self, img1, img2, img3, img4, img5, img6, img7, img8, img9, img10):
        self.img1 = img1
        self.img2 = img2
        self.img3 = img3
        self.img4 = img4
        self.img5 = img5
        self.img6 = img6
        self.img7 = img7
        self.img8 = img8
        self.img9 = img9
        self.img10 = img10

        self.IMG_MAX = list([
            pg.image.load(self.img1),pg.image.load(self.img1),
            pg.image.load(self.img2),pg.image.load(self.img2),
            pg.image.load(self.img3),pg.image.load(self.img3),
            pg.image.load(self.img4),pg.image.load(self.img4),
            pg.image.load(self.img5),pg.image.load(self.img5),
            pg.image.load(self.img6),pg.image.load(self.img6),
            pg.image.load(self.img7),pg.image.load(self.img7),
            pg.image.load(self.img8),pg.image.load(self.img8),
            pg.image.load(self.img9),pg.image.load(self.img9),
            pg.image.load(self.img10),pg.image.load(self.img10)
        ])

    def imgs(self, frame, w, h):
        imgs = []
        for img in self.IMG_MAX:
            imgs.append(pg.transform.scale(img, (w, h)))
        return imgs[frame]

    def imgs_reverse(self, frame, w, h):
        imgs_reverse = []
        for img in self.IMG_MAX:
            imgs_reverse.append(pg.transform.flip(pg.transform.scale(img, (w, h)), 1, 0))
        return imgs_reverse[frame]
