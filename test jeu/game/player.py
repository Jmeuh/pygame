from avatar import *

class Player(Avatar):

    def __init__(self,
        POS_X, POS_Y, MOVE_X, MOVE_Y, x_movement, y_movement, FRAME_COUNT,
        frame, _direction, _attack, _attack_kunai, _slide, _kunai, DEGREES
    ):
        Avatar.__init__(self,
            POS_X, POS_Y, MOVE_X, MOVE_Y, x_movement, y_movement, FRAME_COUNT,
            frame, _direction, _attack, _attack_kunai, _slide, _kunai, DEGREES
        )

    def event(self, e):
        if e.type == pg.KEYDOWN:
            if e.key == pg.K_RIGHT:
                self.x_movement = self.MOVE_X
                self._direction = True
            if e.key == pg.K_LEFT:
                self.x_movement = -self.MOVE_X
                self._direction = False
            if e.key == pg.K_DOWN:
                self._slide = True
            if e.key == pg.K_c:
                self._attack = True
                if self.p_y == self.POS_Y:
                    self.x_movement = 0
            if e.key == pg.K_x:
                self._attack_kunai = True
                self._kunai = True
                if self._direction:
                    self.KUNAI_X, self.KUNAI_Y = self.p_x + 65, self.p_y + 40
                    self.kunai_x, self.kunai_y = self.KUNAI_X, self.KUNAI_Y
                else:
                    self.KUNAI_X, self.KUNAI_Y = self.p_x, self.p_y + 40
                    self.kunai_x, self.kunai_y = self.KUNAI_X, self.KUNAI_Y
                if self.p_y == self.POS_Y:
                    self.x_movement = 0
            if e.key == pg.K_SPACE:
                if self.p_y >= self.POS_Y:
                    self.y_movement = self.MOVE_Y
        if e.type == pg.KEYUP:
            if e.key == pg.K_RIGHT:
                if self.x_movement == self.MOVE_X:
                    self.x_movement = 0
            if e.key == pg.K_LEFT:
                if self.x_movement == -self.MOVE_X:
                    self.x_movement = 0
            if e.key == pg.K_DOWN:
                self._slide = False
            if e.key == pg.K_SPACE:
                self.y_movement = -self.MOVE_Y
