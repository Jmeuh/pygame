from movement import *

player_idle = Sprite(
    'Idle__000.png', 'Idle__001.png', 'Idle__002.png', 'Idle__003.png',
    'Idle__004.png', 'Idle__005.png', 'Idle__006.png', 'Idle__007.png',
    'Idle__008.png', 'Idle__009.png'
)

player_run = Sprite(
    'Run__000.png', 'Run__001.png', 'Run__002.png', 'Run__003.png',
    'Run__004.png', 'Run__005.png', 'Run__006.png', 'Run__007.png',
    'Run__008.png', 'Run__009.png'
)

player_jump = Sprite(
    'Jump__000.png', 'Jump__001.png', 'Jump__002.png', 'Jump__003.png',
    'Jump__004.png', 'Jump__005.png', 'Jump__006.png', 'Jump__007.png',
    'Jump__008.png', 'Jump__009.png'
)

player_jump_throw = Sprite(
    'Jump_Throw__000.png', 'Jump_Throw__001.png', 'Jump_Throw__002.png', 'Jump_Throw__003.png',
    'Jump_Throw__004.png', 'Jump_Throw__005.png', 'Jump_Throw__006.png', 'Jump_Throw__007.png',
    'Jump_Throw__008.png', 'Jump_Throw__009.png'
)

player_jump_attack = Sprite(
    'Jump_Attack__000.png', 'Jump_Attack__001.png', 'Jump_Attack__002.png', 'Jump_Attack__003.png',
    'Jump_Attack__004.png', 'Jump_Attack__005.png', 'Jump_Attack__006.png', 'Jump_Attack__007.png',
    'Jump_Attack__008.png', 'Jump_Attack__009.png'
)

player_slide = Sprite(
    'Slide__000.png', 'Slide__001.png', 'Slide__002.png', 'Slide__003.png',
    'Slide__004.png', 'Slide__005.png', 'Slide__006.png', 'Slide__007.png',
    'Slide__008.png', 'Slide__009.png'
)

player_throw = Sprite(
    'Throw__000.png', 'Throw__001.png', 'Throw__002.png', 'Throw__003.png',
    'Throw__004.png', 'Throw__005.png', 'Throw__006.png', 'Throw__007.png',
    'Throw__008.png', 'Throw__009.png'
)

player_attack = Sprite(
    'Attack__000.png', 'Attack__001.png', 'Attack__002.png', 'Attack__003.png',
    'Attack__004.png', 'Attack__005.png', 'Attack__006.png', 'Attack__007.png',
    'Attack__008.png', 'Attack__009.png'
)
