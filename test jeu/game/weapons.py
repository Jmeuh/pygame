import pygame as pg

KUNAI_MAX = pg.image.load('Kunai.png')
KUNAI = pg.transform.scale(KUNAI_MAX, (7, 34))
RECT = KUNAI.get_rect()

def rotate_kunai(surface, x, y, image, degrees=180):
    rotated = pg.transform.rotate(image, degrees)
    rect = rotated.get_rect()
    surface.blit(rotated, (x - rect.center[0], y - rect.center[1]))
