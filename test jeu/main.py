import pygame as pg
import time
import random
from pygame.locals import *

grey = (39, 39, 42)
blue = (113,177,227)
white = (255,255,255)

pg.init()

surfaceW = 1240
surfaceH = 700

surface = pg.display.set_mode((surfaceW, surfaceH))
clock = pg.time.Clock()
pg.display.set_caption("Test de déplacement")

idle_max = list([
    pg.image.load('Idle__000.png'), pg.image.load('Idle__001.png'),
    pg.image.load('Idle__002.png'), pg.image.load('Idle__003.png'),
    pg.image.load('Idle__004.png'), pg.image.load('Idle__005.png'),
    pg.image.load('Idle__006.png'), pg.image.load('Idle__007.png'),
    pg.image.load('Idle__008.png'), pg.image.load('Idle__009.png')
])

idle = list([
    pg.transform.scale(idle_max[0], (50, 90)),
    pg.transform.scale(idle_max[0], (50, 90)),
    pg.transform.scale(idle_max[1], (50, 90)),
    pg.transform.scale(idle_max[1], (50, 90)),
    pg.transform.scale(idle_max[2], (50, 90)),
    pg.transform.scale(idle_max[2], (50, 90)),
    pg.transform.scale(idle_max[3], (50, 90)),
    pg.transform.scale(idle_max[3], (50, 90)),
    pg.transform.scale(idle_max[4], (50, 90)),
    pg.transform.scale(idle_max[4], (50, 90)),
    pg.transform.scale(idle_max[5], (50, 90)),
    pg.transform.scale(idle_max[5], (50, 90)),
    pg.transform.scale(idle_max[6], (50, 90)),
    pg.transform.scale(idle_max[6], (50, 90)),
    pg.transform.scale(idle_max[7], (50, 90)),
    pg.transform.scale(idle_max[7], (50, 90)),
    pg.transform.scale(idle_max[8], (50, 90)),
    pg.transform.scale(idle_max[8], (50, 90)),
    pg.transform.scale(idle_max[9], (50, 90)),
    pg.transform.scale(idle_max[9], (50, 90))
])

idle_reverse = list([
    pg.transform.flip(idle[0], 1, 0), pg.transform.flip(idle[1], 1, 0),
    pg.transform.flip(idle[2], 1, 0), pg.transform.flip(idle[3], 1, 0),
    pg.transform.flip(idle[4], 1, 0), pg.transform.flip(idle[5], 1, 0),
    pg.transform.flip(idle[6], 1, 0), pg.transform.flip(idle[7], 1, 0),
    pg.transform.flip(idle[8], 1, 0), pg.transform.flip(idle[9], 1, 0),
    pg.transform.flip(idle[10], 1, 0), pg.transform.flip(idle[11], 1, 0),
    pg.transform.flip(idle[12], 1, 0), pg.transform.flip(idle[13], 1, 0),
    pg.transform.flip(idle[14], 1, 0), pg.transform.flip(idle[15], 1, 0),
    pg.transform.flip(idle[16], 1, 0), pg.transform.flip(idle[17], 1, 0),
    pg.transform.flip(idle[18], 1, 0), pg.transform.flip(idle[19], 1, 0)
])

run_max = list([
    pg.image.load('Run__000.png'), pg.image.load('Run__001.png'),
    pg.image.load('Run__002.png'), pg.image.load('Run__003.png'),
    pg.image.load('Run__004.png'), pg.image.load('Run__005.png'),
    pg.image.load('Run__006.png'), pg.image.load('Run__007.png'),
    pg.image.load('Run__008.png'), pg.image.load('Run__009.png')
])

run = list([
    pg.transform.scale(run_max[0], (75, 90)),
    pg.transform.scale(run_max[0], (75, 90)),
    pg.transform.scale(run_max[1], (75, 90)),
    pg.transform.scale(run_max[1], (75, 90)),
    pg.transform.scale(run_max[2], (75, 90)),
    pg.transform.scale(run_max[2], (75, 90)),
    pg.transform.scale(run_max[3], (75, 90)),
    pg.transform.scale(run_max[3], (75, 90)),
    pg.transform.scale(run_max[4], (75, 90)),
    pg.transform.scale(run_max[4], (75, 90)),
    pg.transform.scale(run_max[5], (75, 90)),
    pg.transform.scale(run_max[5], (75, 90)),
    pg.transform.scale(run_max[6], (75, 90)),
    pg.transform.scale(run_max[6], (75, 90)),
    pg.transform.scale(run_max[7], (75, 90)),
    pg.transform.scale(run_max[7], (75, 90)),
    pg.transform.scale(run_max[8], (75, 90)),
    pg.transform.scale(run_max[8], (75, 90)),
    pg.transform.scale(run_max[9], (75, 90)),
    pg.transform.scale(run_max[9], (75, 90))
])

run_reverse = list([
    pg.transform.flip(run[0], 1, 0), pg.transform.flip(run[1], 1, 0),
    pg.transform.flip(run[2], 1, 0), pg.transform.flip(run[3], 1, 0),
    pg.transform.flip(run[4], 1, 0), pg.transform.flip(run[5], 1, 0),
    pg.transform.flip(run[6], 1, 0), pg.transform.flip(run[7], 1, 0),
    pg.transform.flip(run[8], 1, 0), pg.transform.flip(run[9], 1, 0),
    pg.transform.flip(run[10], 1, 0), pg.transform.flip(run[11], 1, 0),
    pg.transform.flip(run[12], 1, 0), pg.transform.flip(run[13], 1, 0),
    pg.transform.flip(run[14], 1, 0), pg.transform.flip(run[15], 1, 0),
    pg.transform.flip(run[16], 1, 0), pg.transform.flip(run[17], 1, 0),
    pg.transform.flip(run[18], 1, 0), pg.transform.flip(run[19], 1, 0)
])


# surface.blit(idle,(590, 560))

jump_max = list([
    pg.image.load('Jump__000.png'), pg.image.load('Jump__001.png'),
    pg.image.load('Jump__002.png'), pg.image.load('Jump__003.png'),
    pg.image.load('Jump__004.png'), pg.image.load('Jump__005.png'),
    pg.image.load('Jump__006.png'), pg.image.load('Jump__007.png'),
    pg.image.load('Jump__008.png'), pg.image.load('Jump__009.png')
])
jump = list([
    pg.transform.scale(jump_max[0], (75, 107)),
    pg.transform.scale(jump_max[0], (75, 107)),
    pg.transform.scale(jump_max[1], (75, 107)),
    pg.transform.scale(jump_max[1], (75, 107)),
    pg.transform.scale(jump_max[2], (75, 107)),
    pg.transform.scale(jump_max[2], (75, 107)),
    pg.transform.scale(jump_max[3], (75, 107)),
    pg.transform.scale(jump_max[3], (75, 107)),
    pg.transform.scale(jump_max[4], (75, 107)),
    pg.transform.scale(jump_max[4], (75, 107)),
    pg.transform.scale(jump_max[5], (75, 107)),
    pg.transform.scale(jump_max[5], (75, 107)),
    pg.transform.scale(jump_max[6], (75, 107)),
    pg.transform.scale(jump_max[6], (75, 107)),
    pg.transform.scale(jump_max[7], (75, 107)),
    pg.transform.scale(jump_max[7], (75, 107)),
    pg.transform.scale(jump_max[8], (75, 107)),
    pg.transform.scale(jump_max[8], (75, 107)),
    pg.transform.scale(jump_max[9], (75, 107)),
    pg.transform.scale(jump_max[9], (75, 107))
])
jump_reverse = list([
    pg.transform.flip(jump[0], 1, 0), pg.transform.flip(jump[1], 1, 0),
    pg.transform.flip(jump[2], 1, 0), pg.transform.flip(jump[3], 1, 0),
    pg.transform.flip(jump[4], 1, 0), pg.transform.flip(jump[5], 1, 0),
    pg.transform.flip(jump[6], 1, 0), pg.transform.flip(jump[7], 1, 0),
    pg.transform.flip(jump[8], 1, 0), pg.transform.flip(jump[9], 1, 0),
    pg.transform.flip(jump[10], 1, 0), pg.transform.flip(jump[11], 1, 0),
    pg.transform.flip(jump[12], 1, 0), pg.transform.flip(jump[13], 1, 0),
    pg.transform.flip(jump[14], 1, 0), pg.transform.flip(jump[15], 1, 0),
    pg.transform.flip(jump[16], 1, 0), pg.transform.flip(jump[17], 1, 0),
    pg.transform.flip(jump[18], 1, 0), pg.transform.flip(jump[19], 1, 0)
])

slide_max = list([
    pg.image.load('Slide__000.png'), pg.image.load('Slide__001.png'),
    pg.image.load('Slide__002.png'), pg.image.load('Slide__003.png'),
    pg.image.load('Slide__004.png'), pg.image.load('Slide__005.png'),
    pg.image.load('Slide__006.png'), pg.image.load('Slide__007.png'),
    pg.image.load('Slide__008.png'), pg.image.load('Slide__009.png')
])
slide = list([
    pg.transform.scale(slide_max[0], (80, 75)),
    pg.transform.scale(slide_max[0], (80, 75)),
    pg.transform.scale(slide_max[1], (80, 75)),
    pg.transform.scale(slide_max[1], (80, 75)),
    pg.transform.scale(slide_max[2], (80, 75)),
    pg.transform.scale(slide_max[2], (80, 75)),
    pg.transform.scale(slide_max[3], (80, 75)),
    pg.transform.scale(slide_max[3], (80, 75)),
    pg.transform.scale(slide_max[4], (80, 75)),
    pg.transform.scale(slide_max[4], (80, 75)),
    pg.transform.scale(slide_max[5], (80, 75)),
    pg.transform.scale(slide_max[5], (80, 75)),
    pg.transform.scale(slide_max[6], (80, 75)),
    pg.transform.scale(slide_max[6], (80, 75)),
    pg.transform.scale(slide_max[7], (80, 75)),
    pg.transform.scale(slide_max[7], (80, 75)),
    pg.transform.scale(slide_max[8], (80, 75)),
    pg.transform.scale(slide_max[8], (80, 75)),
    pg.transform.scale(slide_max[9], (80, 75)),
    pg.transform.scale(slide_max[9], (80, 75))
])
slide_reverse = list([
    pg.transform.flip(slide[0], 1, 0), pg.transform.flip(slide[1], 1, 0),
    pg.transform.flip(slide[2], 1, 0), pg.transform.flip(slide[3], 1, 0),
    pg.transform.flip(slide[4], 1, 0), pg.transform.flip(slide[5], 1, 0),
    pg.transform.flip(slide[6], 1, 0), pg.transform.flip(slide[7], 1, 0),
    pg.transform.flip(slide[8], 1, 0), pg.transform.flip(slide[9], 1, 0),
    pg.transform.flip(slide[10], 1, 0), pg.transform.flip(slide[11], 1, 0),
    pg.transform.flip(slide[12], 1, 0), pg.transform.flip(slide[13], 1, 0),
    pg.transform.flip(slide[14], 1, 0), pg.transform.flip(slide[15], 1, 0),
    pg.transform.flip(slide[16], 1, 0), pg.transform.flip(slide[17], 1, 0),
    pg.transform.flip(slide[18], 1, 0), pg.transform.flip(slide[19], 1, 0)
])

throw_max = list([
    pg.image.load('Throw__000.png'), pg.image.load('Throw__001.png'),
    pg.image.load('Throw__002.png'), pg.image.load('Throw__003.png'),
    pg.image.load('Throw__004.png'), pg.image.load('Throw__005.png'),
    pg.image.load('Throw__006.png'), pg.image.load('Throw__007.png'),
    pg.image.load('Throw__008.png'), pg.image.load('Throw__009.png')
])
throw = list([
    pg.transform.scale(throw_max[0], (75, 90)),
    pg.transform.scale(throw_max[0], (75, 90)),
    pg.transform.scale(throw_max[1], (75, 90)),
    pg.transform.scale(throw_max[1], (75, 90)),
    pg.transform.scale(throw_max[2], (75, 90)),
    pg.transform.scale(throw_max[2], (75, 90)),
    pg.transform.scale(throw_max[3], (75, 90)),
    pg.transform.scale(throw_max[3], (75, 90)),
    pg.transform.scale(throw_max[4], (75, 90)),
    pg.transform.scale(throw_max[4], (75, 90)),
    pg.transform.scale(throw_max[5], (75, 90)),
    pg.transform.scale(throw_max[5], (75, 90)),
    pg.transform.scale(throw_max[6], (75, 90)),
    pg.transform.scale(throw_max[6], (75, 90)),
    pg.transform.scale(throw_max[7], (75, 90)),
    pg.transform.scale(throw_max[7], (75, 90)),
    pg.transform.scale(throw_max[8], (75, 90)),
    pg.transform.scale(throw_max[8], (75, 90)),
    pg.transform.scale(throw_max[9], (75, 90)),
    pg.transform.scale(throw_max[9], (75, 90))
])
throw_reverse = list([
    pg.transform.flip(throw[0], 1, 0), pg.transform.flip(throw[1], 1, 0),
    pg.transform.flip(throw[2], 1, 0), pg.transform.flip(throw[3], 1, 0),
    pg.transform.flip(throw[4], 1, 0), pg.transform.flip(throw[5], 1, 0),
    pg.transform.flip(throw[6], 1, 0), pg.transform.flip(throw[7], 1, 0),
    pg.transform.flip(throw[8], 1, 0), pg.transform.flip(throw[9], 1, 0),
    pg.transform.flip(throw[10], 1, 0), pg.transform.flip(throw[11], 1, 0),
    pg.transform.flip(throw[12], 1, 0), pg.transform.flip(throw[13], 1, 0),
    pg.transform.flip(throw[14], 1, 0), pg.transform.flip(throw[15], 1, 0),
    pg.transform.flip(throw[16], 1, 0), pg.transform.flip(throw[17], 1, 0),
    pg.transform.flip(throw[18], 1, 0), pg.transform.flip(throw[19], 1, 0)
])

attack_max = list([
    pg.image.load('Attack__000.png'), pg.image.load('Attack__001.png'),
    pg.image.load('Attack__002.png'), pg.image.load('Attack__003.png'),
    pg.image.load('Attack__004.png'), pg.image.load('Attack__005.png'),
    pg.image.load('Attack__006.png'), pg.image.load('Attack__007.png'),
    pg.image.load('Attack__008.png'), pg.image.load('Attack__009.png')
])
attack = list([
    pg.transform.scale(attack_max[0], (105, 100)),
    pg.transform.scale(attack_max[0], (105, 100)),
    pg.transform.scale(attack_max[1], (105, 100)),
    pg.transform.scale(attack_max[1], (105, 100)),
    pg.transform.scale(attack_max[2], (105, 100)),
    pg.transform.scale(attack_max[2], (105, 100)),
    pg.transform.scale(attack_max[3], (105, 100)),
    pg.transform.scale(attack_max[3], (105, 100)),
    pg.transform.scale(attack_max[4], (105, 100)),
    pg.transform.scale(attack_max[4], (105, 100)),
    pg.transform.scale(attack_max[5], (105, 100)),
    pg.transform.scale(attack_max[5], (105, 100)),
    pg.transform.scale(attack_max[6], (105, 100)),
    pg.transform.scale(attack_max[6], (105, 100)),
    pg.transform.scale(attack_max[7], (105, 100)),
    pg.transform.scale(attack_max[7], (105, 100)),
    pg.transform.scale(attack_max[8], (105, 100)),
    pg.transform.scale(attack_max[8], (105, 100)),
    pg.transform.scale(attack_max[9], (105, 100)),
    pg.transform.scale(attack_max[9], (105, 100))
])
attack_reverse = list([
    pg.transform.flip(attack[0], 1, 0), pg.transform.flip(attack[1], 1, 0),
    pg.transform.flip(attack[2], 1, 0), pg.transform.flip(attack[3], 1, 0),
    pg.transform.flip(attack[4], 1, 0), pg.transform.flip(attack[5], 1, 0),
    pg.transform.flip(attack[6], 1, 0), pg.transform.flip(attack[7], 1, 0),
    pg.transform.flip(attack[8], 1, 0), pg.transform.flip(attack[9], 1, 0),
    pg.transform.flip(attack[10], 1, 0), pg.transform.flip(attack[11], 1, 0),
    pg.transform.flip(attack[12], 1, 0), pg.transform.flip(attack[13], 1, 0),
    pg.transform.flip(attack[14], 1, 0), pg.transform.flip(attack[15], 1, 0),
    pg.transform.flip(attack[16], 1, 0), pg.transform.flip(attack[17], 1, 0),
    pg.transform.flip(attack[18], 1, 0), pg.transform.flip(attack[19], 1, 0)
])

KUNAI_MAX = pg.image.load('Kunai.png')
KUNAI = pg.transform.scale(KUNAI_MAX, (7, 34))
RECT = KUNAI.get_rect()

def rotate_kunai(surface, x, y, image, degrees):
    rotated = pg.transform.rotate(image, degrees)
    rect = rotated.get_rect()
    surface.blit(rotated, (x - rect.center[0], y - rect.center[1]))

def main():
    degrees = 0
    x_0 = 590
    y_0 = 565
    x = x_0
    y = y_0
    kunai_X = x + 65
    kunai_Y = y + 40
    kunai_x = kunai_X
    kunai_y = kunai_Y
    x_movement = 0
    right_move = 8
    left_move = -8
    slide_begin = 0
    _attack = False
    attack_kunai = False
    kunai = False

    y_movement = 0

    FRAME_COUNT = 20
    KUNAI_FRAME_COUNT = 24
    kunai_frame = 0
    frame = 0
    direction = True
    inSlide = False

    game_over = False

    while not game_over:

        for event in pg.event.get():
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                game_over = True
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_SPACE:
                    if y >= y_0:
                        y_movement = -15
                if event.key == pg.K_RIGHT:
                    if attack_kunai == True:
                        x_movement = 0
                    else:
                        x_movement = right_move
                        direction = True
                if event.key == pg.K_LEFT:
                    if attack_kunai == True:
                        x_movement = 0
                    else:
                        x_movement = left_move
                        direction = False
                if event.key == pg.K_DOWN:
                    inSlide = True
                if event.key == pg.K_d:
                    attack_kunai = True
                    kunai = True
                    x_movement = 0
                if event.key == pg.K_c:
                    _attack = True
                    x_movement = 0
            if event.type == pg.KEYUP:
                if event.key == pg.K_SPACE:
                    y_movement = 8
                if event.key == pg.K_LEFT:
                    if x_movement == -8:
                        x_movement = 0
                if event.key == pg.K_RIGHT:
                    if x_movement == 8:
                        x_movement = 0
                if event.key == pg.K_DOWN:
                    inSlide = False

        x += x_movement
        y += y_movement

        frame += 1
        if frame > FRAME_COUNT - 1:
            frame = 0

        # kunai_frame += 1
        # if kunai_frame > KUNAI_FRAME_COUNT - 1:
        #     kunai_frame = 0

        if y < surfaceH - 300:
            y_movement = 15
        if y >= surfaceH - 140:
            y = y_0
            y_movement = 0

        surface.fill(blue)
        pg.draw.rect(surface, (39,155,42), (0, 650, 1240, 50))

        mov = [
            idle[frame], idle_reverse[frame],
            jump[frame], jump_reverse[frame],
            run[frame], run_reverse[frame],
            slide[frame], slide_reverse[frame],
            throw[frame], throw_reverse[frame],
            attack[frame], attack_reverse[frame]
        ]
        if direction:
            player = mov[0]
            if y < 560:
                player = mov[2]
            if x_movement > 0 and y == y_0:
                player = mov[4]
                y += 1
                if inSlide:
                    player = mov[6]
                    slide_begin = x
                    y += 24
                    # if x == slide_begin + 50:
                    #     inSlide = False
            if _attack:
                player = mov[10]
                if frame == FRAME_COUNT - 1:
                    _attack = False
            if attack_kunai:
                player = mov[8]
                if frame == FRAME_COUNT - 1:
                    attack_kunai = False

            if kunai:
                rotate_kunai(surface, kunai_x, kunai_y, KUNAI, degrees)
                kunai_x += 100
                degrees += 150
                if degrees == 360:
                    degrees = 0
                if kunai_x >= surfaceW:
                    kunai_x = surfaceW
                    kunai = False
                    kunai_x = kunai_X

        else:
            player = mov[1]
            if y < 560:
                player = mov[3]
            if x_movement < 0:
                player = mov[5]
                y += 1
                if inSlide:
                    player = mov[7]
                    y += 24
            if _attack:
                x -= 30
                x_movement = 0
                player = mov[11]
                if frame == FRAME_COUNT - 1:
                    _attack = False
            if attack_kunai:
                player = mov[9]
                if frame == FRAME_COUNT - 1:
                    attack_kunai = False

        surface.blit(player,(x, y))

        pg.display.update()
        clock.tick(30)

main()
pg.quit()
quit()
